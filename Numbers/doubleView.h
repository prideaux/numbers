//
//  doubleView.h
//  Numbers
//
//  Created by Glen Prideaux on 11/4/17.
//
//

#import <Foundation/Foundation.h>

@interface doubleView : UIViewController

- (void)adjustToSize: (CGSize)size;
@property (nonatomic, retain) IBOutlet UIView *upperView;
@property (nonatomic, retain) IBOutlet UIView *lowerView;
@property (nonatomic, retain) IBOutlet UIView *enclosingView;
@end
