//
//  NumbersSolver.m
//  Numbers
//
//  Created by Glen Prideaux on 21/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "NumbersSolver.h"
#import "Lily.h"
#define TIMEOUT 60
#define TICK 0.05

@implementation NumbersSolver
+ (void)addShadowTo: (UIView*)view
{
    [view.layer setShadowOffset:CGSizeMake(5, 5)];
    [view.layer setShadowColor:[[UIColor systemGrayColor] CGColor]];
    [view.layer setShadowOpacity:0.5];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // initialize card labels
    [NumbersSolver addShadowTo: _cmdAsk];
    [NumbersSolver addShadowTo: _cmdStop];
}

- (void)dealloc
{
    [_numbersPicker release];
    [_textTarget release];
    [_stepperTarget release];
    [_sliderTarget release];
    [_textAnswers release];
    [_cmdAsk release];
    [_cmdStop release];
    [_activityIndicator release];
    [super dealloc];
}

- (IBAction)targetChanged:(id)sender
{
    if (sender!=_sliderTarget) {
        _sliderTarget.value=(float)_stepperTarget.value;
    } else {
        _stepperTarget.value=(double)_sliderTarget.value;
    }
    _textTarget.text=[NSString stringWithFormat:@"%d", (int)_stepperTarget.value];
}

- (IBAction)askLili:(id)sender
{
    Lily *myLili=[Lily new];
    myLili.target=(int)self.stepperTarget.value;
    myLili.numbers=[self.numbersPicker getNumbers];
    myLili.timeout=TIMEOUT;
    LilyAttempt *bestYet=nil;
    [self.cmdAsk setUserInteractionEnabled:false];
    //[self.activityIndicator setHidden:false];
    [self.activityIndicator startAnimating];
    [self.cmdStop setHidden:false];
    [self setRunning: true];
    // run function lili in the background, passing the lily object myLili.
    // This will set self.running to false when it finishes
    [self performSelectorInBackground:@selector(lili:) withObject:myLili];
    // For as long as lili has not turned off the running flag,
    while (self.running) {
        // wait a tick
        [[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:(NSTimeInterval)TICK]];
        // if we have a new bestBranch, report it
        LilyAttempt* best;
        best=[myLili.bestBranch count]>0?[myLili.bestBranch objectAtIndex:[myLili.bestBranch count]-1]:nil;
        if (bestYet!=best) {
            bestYet=best;
            [self appendAnswer:[bestYet.soFar stringByAppendingFormat:@"=%d ...", bestYet.total] isBold:false];
        }
    }
    // The running flag has been turned off, so abort Lily if she's still working
    [myLili setAbort:true];
    // Report the final answer (if Lily got a final answer, otherwise say "Stopped")
    [self appendAnswer:myLili.answer isBold:true];
    [self.activityIndicator stopAnimating];
    [self.cmdAsk setUserInteractionEnabled:true];
    [self.cmdStop setHidden:true];
    [myLili release];
}
- (IBAction)stopLili:(id)sender
{
    [self setRunning:false];
}
- (void) lili: (id)param 
{
    Lily *myLili=(Lily*)param;
    [myLili seek];
    [self setRunning:false];
}
- (void) appendAnswer: (NSString*) answer isBold: (BOOL) bold
{
    if(!answer) answer=@"[Stopped]\n";
    NSMutableAttributedString *mutableAttString = [[NSMutableAttributedString alloc] initWithAttributedString:self.textAnswers.attributedText];

    // Create the attributes
    const CGFloat fontSize = 13;
    
    NSDictionary *attributes = @{
        NSFontAttributeName:(bold?[UIFont boldSystemFontOfSize:fontSize]:[UIFont systemFontOfSize:fontSize]),
        NSForegroundColorAttributeName:[UIColor labelColor]
        }; // ... a dictionary with your attributes.
    NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:[answer stringByAppendingString:@"\n"] attributes:attributes];

    [mutableAttString appendAttributedString:newAttString];
    [self.textAnswers setAttributedText:mutableAttString];
    NSRange end;
    end.length=0;
    end.location=[self.textAnswers.text length];
    [self.textAnswers scrollRangeToVisible:end];
}

@end
