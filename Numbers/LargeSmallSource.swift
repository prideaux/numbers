//  Converted to Swift 5.2 by Swiftify v5.2.29435 - https://swiftify.com/
//
//  LargeSmallSource.swift
//  Numbers
//
//  Created by Glen Prideaux on 20/8/20.
//
import Foundation
import UIKit

class LargeSmallSource: NSObject {
    override init() {
        super.init()
        // Initialization code here.
    }

    func numberOfComponents(in: UIPickerView?) -> Int {
        // only ever a single component
        return 1
    }

    func pickerView(_: UIPickerView, numberOfRowsInComponent: Int) -> Int {
        // we have five rows
        return 5
    }
}
