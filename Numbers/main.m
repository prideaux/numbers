//
//  main.m
//  Numbers
//
//  Created by Glen Prideaux on 16/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NumbersAppDelegate.h"

int main(int argc, char *argv[])
{
  
    @autoreleasepool {

        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NumbersAppDelegate class]));

    }
/*
 * pre-storyboard versio
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
 */
}
