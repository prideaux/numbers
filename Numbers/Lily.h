//
//  Lily.h
//  Numbers
//
//  Created by Glen Prideaux on 19/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface LilyAttempt : NSObject
// the mathematical expression so far
@property (nonatomic, copy) NSString *soFar;
// the last operation was + or - so if the next is * or / we'll need brackets
@property (nonatomic) BOOL mayNeedBrackets;
// the total so far
@property (nonatomic) int total;
// what numbers have we used?
@property (nonatomic) unsigned int numbersUsed;
@property (nonatomic) BOOL isSingleNumber;

- (LilyAttempt*) copyPlus:(LilyAttempt*) operand;
- (LilyAttempt*) copyTimes:(LilyAttempt*) operand;
- (LilyAttempt*) copyDifference:(LilyAttempt*) operand;
- (LilyAttempt*) copyQuotient:(LilyAttempt*) operand;
- (NSArray*) newPairingsWith: (LilyAttempt*) operand;

@end


@interface Lily : NSObject 
// the target
@property (nonatomic) int target;
// what numbers do we have to work with?
@property (nonatomic, retain) NSArray *numbers;
@property (nonatomic) BOOL abort;
@property (nonatomic) int timeout;
@property (nonatomic, retain) NSArray* bestBranch;
@property (nonatomic, retain) NSString *answer;
@property BOOL findAll;
@property unsigned int bestError;

- (NSString*) seek;
- (BOOL) checkBest:(LilyAttempt*) attempt;

@end

