//
//  NumbersPicker.h
//  Numbers
//
//  Created by Glen Prideaux on 21/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
@interface myCounter : NSObject 
@property (nonatomic) NSInteger integerValue;
+ (myCounter*) newCounterWithInteger: (NSInteger) value;
//- (myCounter*) inc;
- (myCounter*) dec;
@end
*/

@interface NumbersPicker : NSObject
@property (nonatomic, retain) IBOutlet UIPickerView *numbersPicker;
@property (nonatomic, retain) NSMutableArray *componentRows;
@property (nonatomic) int *fullDeck;
+ (int) indexFromNumber: (int) number;
+ (int) numberFromIndex: (int) index;
- (void) makeFullDeck;
- (void) cloneFullDeckTo: (int*)target;
- (void) makeComponentRowsFrom:(int)start;
- (NSNumber*) getNumberForComponent: (NSInteger)component;
- (NSArray*) getNumbers;
@end
