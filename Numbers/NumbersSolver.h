//
//  NumbersSolver.h
//  Numbers
//
//  Created by Glen Prideaux on 21/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NumbersPicker.h"

@interface NumbersSolver : UIViewController
@property (nonatomic, retain) IBOutlet NumbersPicker *numbersPicker;
@property (nonatomic, retain) IBOutlet UITextField *textTarget;
@property (nonatomic, retain) IBOutlet UIStepper *stepperTarget;
@property (nonatomic, retain) IBOutlet UISlider *sliderTarget;
@property (nonatomic, retain) IBOutlet UITextView *textAnswers;
@property (nonatomic, retain) IBOutlet UIButton *cmdAsk;
@property (nonatomic, retain) IBOutlet UIButton *cmdStop;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic) BOOL running;
+ (void)addShadowTo: (UIView*)view;
- (IBAction)targetChanged:(id)sender;
- (IBAction)askLili:(id)sender;
- (IBAction)stopLili:(id)sender;
- (void) lili: (id)param;
- (void) appendAnswer: (NSString*) answer isBold: (BOOL) bold;
@end
