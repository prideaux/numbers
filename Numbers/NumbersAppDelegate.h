//
//  NumbersAppDelegate.h
//  Numbers
//
//  Created by Glen Prideaux on 16/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Lily.h"
#import "doubleView.h"
#import "NumbersViewController.h"
//@class NumbersViewController;

@interface NumbersAppDelegate : NSObject <UIApplicationDelegate> 

@property (nonatomic, retain) NumbersViewController* mainController;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UIView *solverView;
@property (nonatomic, retain) IBOutlet UIButton *cmdAsk;


@end
