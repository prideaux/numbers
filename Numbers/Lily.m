//
//  Lily.m
//  Numbers
//
//  Created by Glen Prideaux on 19/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

// Given six numbers and a target, find a mathematical expression to get
// as close as possible to the target.

#import "Lily.h"

@implementation Lily
@synthesize target, numbers, abort, timeout, bestBranch,answer;
- (void) dealloc
{
    [bestBranch release];
    [numbers release];
    [answer release];
    [super dealloc];
}
- (Lily*) init
{
    self=[super init];
    [self setFindAll: true];
    [self setBestBranch:[NSArray new]];
    return self;
}

// check to see if we've found the best so far.
// bestBranch is an array  ... if it has exactly one element then that's the best
// match found so far. If it has more than one element then they are each perfect
// matches.
// Return value is true only if this attempt is better than any previous.
- (BOOL) checkBest:(LilyAttempt*) attempt
{
    // How far off are we? Record the best attempt so far and which attempt it was
    if(abs(self.target-attempt.total)<_bestError){
        _bestError=abs(attempt.total-self.target);
        [self setBestBranch: [NSArray arrayWithObject: attempt]];
        return true;
    } else {
        // A subsequent perfect match also gets added
        // but only if it's soFar string is unique
        if(self.target == attempt.total) {
            if(NSNotFound == [[self bestBranch] indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                return NSOrderedSame==[((LilyAttempt*)obj).soFar compare: attempt.soFar];
            }])
            [self setBestBranch:[[self bestBranch] arrayByAddingObject:attempt]];
        }
    }
    return false;
}
/* seek
 * Build a tree of attempts, keeping track of the best result so far from any
 * subtrees. Stop when we find an exact match or when we timeout.
 */
- (NSString*) seek
{
    // setup. Set up arrays of branches. Record starting time. Turn off abort flag.
    // Initialize the bestError as a very large number (but anything 900+ would work)
    // (since anything is better than nothing)
    NSMutableArray *branches, *newBranches, *newNewBranches;
    NSDate *start=[NSDate date];
    branches=[NSMutableArray new];
    newBranches=[NSMutableArray new];
    self.abort=false;
    // what's the best total in any branch?
    _bestError=UINT_MAX;
    
    // set up initial branches
    // enumerator to step through the numbers
    NSEnumerator *enumerator=[self.numbers objectEnumerator];
    NSNumber *number;
    // We use bits to indicate which numbers we have used
    unsigned int bit=1;
    // stepping through the numbers ... unless we find a match
    // (the only possible single number match is 100=100, but that is possible)
    // we set up the initial attempts from the six basic numbers ... but skipping
    // any repeats
    while ((number=[enumerator nextObject]) && (_findAll || _bestError!=0)) {
        // We need to include every number as a base attempt, even repeats, so that
        // repeated numbers become available to subtrees
        LilyAttempt *attempt=[LilyAttempt new];
        [attempt setTotal: (int)[number integerValue]];
        [attempt setSoFar: [number stringValue]];
        [attempt setNumbersUsed:bit];
        [attempt setIsSingleNumber: true];
        [attempt setMayNeedBrackets:false];
        // add the base attempt to the collection of branches
        NSLog(@"Base Attempt: %@\n", attempt.soFar);
        [newBranches addObject:attempt];

        // record the best attempt so far
        [self checkBest:attempt];
        
        // reduce the use count by 1. (It will be retained as long as it's in the
        // array of branches, then the memory freed when that array is released.)
        [attempt release];
        bit <<= 1;
    }
    // At this point branches is empty and newBranches has an attempt for each
    // unique number in the original set
    
    // search
    /* This is currently not finding all solutions. Example:
     * 25 50 75 100 5 8 with target 486 is possible:
     * 5x100-(50-8)/(75/25) or 5x100-(50-8)*25/75 but Lily isn't finding it
     * and not timing out, either.
     */
    
    // keep exploring while we find new pairings (attempts that we can combine with
    // an operator).
    // Although we start with simple numbers rather than pairings we need to set
    // this to true so we don't stop before we really get started!
    BOOL newPairingsFound=true;
    NSInteger offset;
    NSInteger dupesCount=0;
    NSEnumerator *branchAEnumerator, *branchBEnumerator, *newPairingsEnumerator, *dupesEnumerator;
    LilyAttempt *branchA, *branchB, *pairing, *dupes;
    NSRange branchBRange;
    NSArray *newPairings;
    NSArray *newBranchesSubset;
    // while there are new parings and we haven't found an exact answer ...
    while (newPairingsFound 
           && (_findAll || _bestError!=0)
           && abs((int)[start timeIntervalSinceNow])<timeout
           && !self.abort) {
        // branches -- array of attempts before the last iteration
        // newBranches -- attempts from the last iteration
        // newNewBranches -- attempts from this iteration
        
        // We're going to stop unless we find at least one new branch
        newPairingsFound=false;
        newNewBranches=[NSMutableArray new];
        // how many branches do we have (before the last iteration)?
        offset=-[branches count];
        // Add new branches found in the last iteration to the array of branches
        // (It's just a list, not a tree ... the idea of a tree gets confusing
        // where the same number can be both a trunk and a twig in different parts
        // of the tree. It may be that it would be more efficient as a tree ...
        // not sure.)
        [branches addObjectsFromArray:newBranches];
        // Iterate through the branches for the first operand ...
        branchAEnumerator=[branches objectEnumerator];
        while ((branchA=[branchAEnumerator nextObject]) && (_findAll || _bestError!=0)) {
            // initially offset is -n where n is how many branches we had before we added the new ones from
            // the last iteration. We use this as a counter so we know when we're up to new attempts.
            // This is so that we can attempt to make new pairings between existing attempts and new attempts,
            // and also between one new attempt and another, but not between two existing attempts (since we've
            // already done them in a prior iteration).
            offset++;
            // offset reaches 0 once we've iterated through all the branches
            // that existed before the last iteration, so >0 means we've reached
            // new attempts.
            // For example, if there were six previous attempts then offset starts at -6 and reaches 0 after
            // it has been incremented a sixth time (i.e. when we're working on the last of the existing attempts)
            // and is then greater than zero on the seventh attempt (i.e. when we're working on the first of the
            // new attempts.
            if(offset>0) {
                // branchA is now one of the new attempts.
                // For branchB we only iterate over the new attempts after this one since any pairing of
                // A&B is identical to a pairing of B&A so there's no need to do both

                branchBRange.location=offset; // offset is now one more than the index of the current branchA
                branchBRange.length=[newBranches count]-offset;
                newBranchesSubset=[newBranches subarrayWithRange:branchBRange]; // all possible branchB attempts
                branchBEnumerator=[newBranchesSubset objectEnumerator];
            } else {
                // we haven't reached the newBranches attempts yet for branchA so
                // we need to iterate over the whole newBranches collection for branchB
                branchBEnumerator=[newBranches objectEnumerator];
            }
            // iterate through branches for the second operand ...
            while ((branchB=[branchBEnumerator nextObject]) 
                   && (_findAll || _bestError!=0)
                   && abs((int)[start timeIntervalSinceNow])<timeout
                   && !self.abort) {
                // We now have branchA and branchB as attempts that have never been joined
                // Try adding, multiplying, subtracting and dividing them, always larger (operator) smaller since
                // + and * don't care and - and / require it done in that order in order to have positive integers only
                newPairings=[branchA newPairingsWith:branchB];
                if(newPairings!=nil && [newPairings count]>0) {
                    newPairingsEnumerator=[newPairings objectEnumerator];
                    while ((pairing=[newPairingsEnumerator nextObject])
                           && (_findAll || _bestError!=0)) {
                        BOOL unique=true;
                        if([self checkBest:pairing]) {
                            // if it's our best yet, it must be unique so we don't need to check
                            NSLog(@"New best: %@=%d (target %d)",pairing.soFar, pairing.total,self.target);
                        } else {
                            /* If we don't kill dupes then it slows right down, the number of attempts skyrockets
                             * with most of them being unhelpful dupes and it doesn't stop, even after the timeout
                             * ... looks like it crashed the simulator. Out of memory? How do I catch that?
                             */
                            dupesEnumerator=[branches objectEnumerator];
                            // An attempt is a duplicate if it uses (a superset of) the
                            // same numbers and achieves the same total.
                            // (We don't care about finding different solutions;
                            // we're only looking for the first.)
                            while(dupes=[dupesEnumerator nextObject]) {
                                if (dupes.total==pairing.total &&
                                    (dupes.numbersUsed & pairing.numbersUsed) == dupes.numbersUsed) {
                                    unique=false;
                                    break;
                                }
                            }
                            // If it's unique amongst previous branches, check that
                            // it's also unique amongst brances in this iteration
                            if (unique) dupesEnumerator=[newNewBranches objectEnumerator];
                            while(unique && (dupes=[dupesEnumerator nextObject])) {
                                if (dupes.total==pairing.total &&
                                    (dupes.numbersUsed & pairing.numbersUsed) == dupes.numbersUsed) {
                                    unique=false;
                                    break;
                                }
                            }
                        }
                        // If it's not a duplicate, add it to this iteration's
                        // collection and set the flag to say we've found something
                        if (unique) {
                            [newNewBranches addObject:pairing];
                            newPairingsFound=true;
                            //NSLog(@"[%lu attempts]",[branches count]+[newNewBranches count]);
                        } else {
                            dupesCount++;
                            //NSLog(@"(%ld dupes)",dupesCount);
                        }
                    }
                    [newPairings release];
                }
            }
        }
        [newBranches release];
        newBranches=newNewBranches;
    }
    NSLog(@"New pairings still found on search loop exit? %@",newPairingsFound?@"Yes":@"No");
    if (self.abort) {
        NSLog(@"Abort encountered in search loop.");
        [branches release];
        [newBranches release];
        return nil;
    }
    NSLog(@"End of search loop %@ time out.",(abs((int)[start timeIntervalSinceNow])<timeout)?@"before":@"after");
        
    //return the best match
    if(_bestError!=0) {
        LilyAttempt* best=((LilyAttempt*)[self.bestBranch objectAtIndex:0]);
        if(-[start timeIntervalSinceNow]<timeout)
            self.answer=[best.soFar stringByAppendingFormat:@"=%d\n(Best I could find)\n", best.total];
        else
            self.answer=[best.soFar stringByAppendingFormat:@"=%d\n(Ran out of time)\n", best.total];
    } else {
        NSEnumerator *bestEnumerator=[self.bestBranch objectEnumerator];
        LilyAttempt *best;
        self.answer=@"";
        while (best=[bestEnumerator nextObject])
            self.answer=[self.answer stringByAppendingFormat:@"%@=%d\n",
                                                            best.soFar,
                                                            best.total];
    }
    [branches release];
    [newBranches release];
    return self.answer;
}

@end

@implementation LilyAttempt
@synthesize soFar, mayNeedBrackets, total, numbersUsed, isSingleNumber;

- (void)dealloc
{
    [soFar release];
    [super dealloc];
}

/* the copy* functions take another attempt and return a new attempt by
 * adding, multiplying, subtracting or dividing if it can be done within
 * the rules (no negatives, no fractions) and makes some sense (no adding
 * or subtracting zero, or indeed subtracting to make zero, no multiplying
 * or dividing by 1, etc.)
 * We build up the soFar string with each operation. This is fairly memory expensive
 * since each branch has its own copy of the string. It may be useful to find a more
 * efficient way of annotating this that gets translated at the end, but for now
 * it is convenient. The possibility of running out of memory is what makes
 * this a less than perfect solver, so improving memory handling may improve Lily.
 */
// Attempt to make a 'pairing' by adding two attempts
- (LilyAttempt*) copyPlus:(LilyAttempt*) operand
{
    // no point in adding zero (but we shouldn't get an attempt with zero)
    if(operand.total==0 || self.total==0) return nil;
    LilyAttempt *result=[LilyAttempt new];
    if (result==nil) { // fail if we couldn't allocate a new attempt (out of memory)
        NSLog(@"Failed to allocate LilyAttempt for %@ plus %@.\n",self.soFar, operand.soFar);
        return nil;
    }
    // We always add a smaller number to a larger
    LilyAttempt *larger, *smaller;
    if(self.total>operand.total) {
        larger=self;
        smaller=operand;
    } else {
        larger=operand;
        smaller=self;
    }
    // Add totals
    [result setTotal: self.total+operand.total];
    // Numbers used is the bitwise OR of the two operands
    [result setNumbersUsed: self.numbersUsed | operand.numbersUsed];
    // Track the string representation
    [result setSoFar: [larger.soFar stringByAppendingFormat:@"+%@",smaller.soFar]];
    // After an addition if the next operation is a multiplication or division we'll need
    // brackets.
    [result setMayNeedBrackets:true];
    // It's not atomic.
    [result setIsSingleNumber: false];
    return result;
}
- (LilyAttempt*) copyTimes:(LilyAttempt*) operand
{
    // no point in multiplying by zero (but we shouldn't get an attempt with zero)
    // also no point in multiplying by one
    if(operand.total==0 || self.total==0 || operand.total==1 || self.total == 1) {
        return nil;
    }
    LilyAttempt *result=[LilyAttempt new];
    if (result==nil) {
        NSLog(@"Failed to allocate LilyAttempt for %@ times %@.\n",self.soFar, operand.soFar);
        return nil;
    }
    // multipy totals
    [result setTotal: self.total*operand.total];
    // Numbers used is the bitwise OR of the two operands
    [result setNumbersUsed: self.numbersUsed | operand.numbersUsed];
    // Add brackets if the previous operation for each attempt was + or -
    if(self.mayNeedBrackets) 
        [result setSoFar:[NSString stringWithFormat: @"(%@)x",self.soFar]];
    else 
        [result setSoFar:[NSString stringWithFormat: @"%@x",self.soFar]];
    if(operand.mayNeedBrackets)
        [result setSoFar:[result.soFar stringByAppendingFormat: @"(%@)",operand.soFar]];
    else
        [result setSoFar:[result.soFar stringByAppendingString:operand.soFar]];
    // next operation won't need brackets
    [result setMayNeedBrackets:false];
    // not atomic
    [result setIsSingleNumber: false];
    return result;
}
- (LilyAttempt*) copyDifference:(LilyAttempt*) operand
{
    // no point in adding zero (but we shouldn't get an attempt with zero)
    // also no point if the difference is zero
    if(operand.total==0 || self.total==0 || self.total==operand.total) return nil;
    // No negatives ... always subtract smaller from larger
    LilyAttempt *larger, *smaller;
    if(self.total>operand.total) {
        larger=self;
        smaller=operand;
    } else {
        larger=operand;
        smaller=self;
    }
    LilyAttempt *result=[LilyAttempt new];
    if (result==nil) {
        NSLog(@"Failed to allocate LilyAttempt for %@ minus %@.\n",self.soFar, operand.soFar);
        return nil; // we might be out of memory
    }
    // subtract totals
    [result setTotal: larger.total-smaller.total];
    // bitwise or of numbers used
    [result setNumbersUsed: self.numbersUsed | operand.numbersUsed];
    // If the last operation was + or - we need brackets
    if(smaller.mayNeedBrackets)
        [result setSoFar:[larger.soFar stringByAppendingFormat: @"-(%@)",smaller.soFar]];
    else
        [result setSoFar:[larger.soFar stringByAppendingFormat: @"-%@",smaller.soFar]];
    // Next operation will need brackets unless it's addition
    [result setMayNeedBrackets:true];
    // not atomic
    [result setIsSingleNumber: false];
    return result;
}
- (LilyAttempt*) copyQuotient:(LilyAttempt*) operand
{
    // can't divide by zero (but we shouldn't get an attempt with zero)
    // also no point if numerator is zero
    // no point if denominator is 1 and no integer quotient if numerator is 1
    if(operand.total==0 || self.total==0 ||
       operand.total==1 || self.total==1) return nil;
    LilyAttempt *larger, *smaller;
    if(self.total>operand.total) {
        larger=self;
        smaller=operand;
    } else {
        larger=operand;
        smaller=self;
    }

    unsigned int q=larger.total/smaller.total;
    if(q*smaller.total!=larger.total) { // not exact -- not allowed
        return nil;
    }
    LilyAttempt *result=[LilyAttempt new];
    if (result==nil) {
        NSLog(@"Failed to allocate LilyAttempt for %@ times %@.\n",self.soFar, operand.soFar);
        return nil; // we might be out of memory
    }
    // result is the integer quotient
    [result setTotal: q];
    // bitwise or of numbers used
    [result setNumbersUsed: self.numbersUsed | operand.numbersUsed];
    // include brackets if we need them
    if(larger.mayNeedBrackets) 
        [result setSoFar:[NSString stringWithFormat: @"(%@)÷",larger.soFar]];
    else 
        [result setSoFar:[NSString stringWithFormat: @"%@÷",larger.soFar]];
    if(smaller.mayNeedBrackets || !smaller.isSingleNumber)
        [result setSoFar:[result.soFar stringByAppendingFormat: @"(%@)",smaller.soFar]];
    else
        [result setSoFar:[result.soFar stringByAppendingString:smaller.soFar]];
    // next operation will not need brackets
    [result setMayNeedBrackets:false];
    // not atomic
    [result setIsSingleNumber: false];
    return result;
}

/* newPairingsWith
 * try adding, subtracting, multiplying and dividing one operand (self) by another
 * Each operand is a LilyAttempt
 * Return an array of the possible pairing results.
 * This can fail if we run out of memory and so fail to allocate a new array to hold
 * the pairings.
 * We return the pairings array with all successful pairings (between 0 and 4) that
 * form new attempts. The pairings array has been retained so must be released by
 * the caller.
 */
- (NSArray*) newPairingsWith: (LilyAttempt*) operand
{
    if((self.numbersUsed & operand.numbersUsed) !=0) // numbers overlap -- not allowed
        return nil;
    NSMutableArray *pairings=[NSMutableArray array];
    if(pairings==nil) {
        NSLog(@"Failed to allocate pairings array for %@ and %@.\n",self.soFar, operand.soFar);
        return nil; // could not allocate memory for the pairings array
    }
    [pairings retain]; // increase use count. The caller becomes responsible for release.
    LilyAttempt *result;
    if((result=[self copyPlus:operand])!=nil) {
        [pairings addObject:result]; [result release];
        //NSLog(@"Attempt +: %@=%d\n", result.soFar,result.total);
    }
    if((result=[self copyTimes:operand])!=nil) {
        [pairings addObject:result]; [result release];
        //NSLog(@"Attempt *: %@=%d\n", result.soFar,result.total);
        
    }
    if((result=[self copyDifference:operand])!=nil) {
        [pairings addObject:result]; [result release];
        //NSLog(@"Attempt -: %@=%d\n", result.soFar,result.total);
        
    }
    if((result=[self copyQuotient:operand])!=nil) {
        [pairings addObject:result]; [result release];
        //NSLog(@"Attempt /: %@=%d\n", result.soFar,result.total);
        
    }
    return pairings;
}
@end
