//
//  NumbersViewController.h
//  Numbers
//
//  Created by Glen Prideaux on 22/8/20.
//

#import <UIKit/UIKit.h>
#import "Lily.h"

NS_ASSUME_NONNULL_BEGIN

@interface NumbersViewController : UIViewController
@property (retain) IBOutlet UIView *cardView;
@property (nonatomic, retain) IBOutlet UIPickerView *nLarge;
@property (retain, nonatomic) IBOutlet UIButton *cmdGo;
@property (retain, nonatomic) IBOutlet UIButton *cmdAsk;
@property (retain, nonatomic) IBOutlet UIView *mainView;
@property (retain, nonatomic) IBOutlet UIView *targetView;
@property (retain, nonatomic) IBOutlet UITextField *txtTarget;
@property (retain, nonatomic) IBOutlet UIProgressView *timer;
@property (nonatomic) unsigned int target;
@property Boolean running;
@property int timerProgress;
@property (retain, nonatomic) Lily* myLili;
@property (nonatomic, retain) NSString *liliSays;
@property (nonatomic, retain) NSMutableArray *numbersForLili;
@property (nonatomic, retain) NSArray *smallCards;
@property (nonatomic, retain) NSArray *largeCards;
@property (nonatomic, retain) NSTimer *actualTimer;

+ (void)addBorderTo: (UIView*)view withSize: (CGFloat) size;
+ (void)addShadowTo: (UIView*)view;
- (void)setCard: (NSInteger)cardNumber to: (NSInteger)value;
- (void)clearCards;
- (NSInteger) getNLarge;
- (void) setTargetTo: (NSString*) value;
- (IBAction)actionGo:(id)sender;
- (void) runTimer;
- (void) resetTimer;
- (void) updateProgress;
- (void) timesUp;
- (IBAction)askLili:(id)sender;
- (NSArray*)loadDeckOfSize: (int) size with:(int) first, ... ;
- (NSArray*)shuffleCards: (NSArray*) cards;
- (void) lili: (id)param;
- (int)getTarget;
NSInteger randSort(id num1, id num2, void *context);

@end
#define TIMERSTEPDELAY 0.5
#define TIMERSTEPS 60
#define LILITIMEOUT 30.0
#define NUMBERDELAY 0.75
#define FLICKERDELAY 0.1
#define FLICKERTIMES 8


NS_ASSUME_NONNULL_END
