//
//  LargeSmallSource.h
//  Numbers
//
//  Created by Glen Prideaux on 16/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

@interface LargeSmallSource : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;

@end

