//
//  NumbersViewController.m
//  Numbers
//
//  Created by Glen Prideaux on 22/8/20.
//

#import "NumbersViewController.h"
#import "NumbersAppDelegate.h"

@interface NumbersViewController ()

@end

@implementation NumbersViewController

+ (void)addBorderTo: (UIView*)view withSize: (CGFloat) size
{
    [view.layer setBorderWidth: size];
    [view.layer setBorderColor:[[UIColor systemGrayColor] CGColor]];
}
+ (void)addShadowTo: (UIView*)view
{
    [view.layer setShadowOffset:CGSizeMake(5, 5)];
    [view.layer setShadowColor:[[UIColor systemGrayColor] CGColor]];
    [view.layer setShadowOpacity:0.5];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // initialize card labels
    for (int slot=0; slot<6; slot++) {
        UILabel *lbl = [[self cardView] viewWithTag: slot];
        [lbl setText:@"-"];
        [NumbersViewController addBorderTo: lbl withSize:1.0];
        }
    
    [NumbersViewController addBorderTo: _cmdAsk withSize:1.0];
    [NumbersViewController addShadowTo: _cmdAsk];
    [NumbersViewController addShadowTo: _cmdGo];
    NumbersAppDelegate *appDelegate = (NumbersAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setMainController:self];
    [self setMyLili:[Lily new]];
    [self setLargeCards: [self loadDeckOfSize: 4 with: 25,50,75,100]];
    [self setSmallCards: [self loadDeckOfSize: 20 with: 1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10]];
    
}

-(void)setCard:(NSInteger)cardNumber to:(NSInteger)value
{
    UILabel *lbl = (UILabel*)[[self cardView] viewWithTag: cardNumber];
    [lbl setText:[NSString stringWithFormat:@"%ld", value]];
}
-(void)clearCards
{
    for (int slot=0; slot<6; slot++) {
        UILabel *lbl = [[self cardView] viewWithTag: slot];
        [lbl setText:@""];
    }

}

- (void) setTargetTo: (NSString*) value
{
    [self.txtTarget setText:value];
}

- (IBAction)actionGo:(id)sender
{
    NSArray *lSmallCards;
    NSArray *lLargeCards;
    // abort any lili stuff that might be going on
    self.myLili.abort=true;
    /* Instance variable running is a flag used to detect that
     * actionGo has been called again before it finished
     * its first invocation. If at any time running is
     * detected to have been reset to false,
     * we exit without any further ado.
     */
    
    [self.cmdAsk setHidden:TRUE];
    self.liliSays=@"";
    self.numbersForLili=[NSMutableArray arrayWithCapacity:6];
    
    _running=true;
    
    // shuffle the two decks and get the number of large and small number cards to draw
    lSmallCards=[self shuffleCards: self.smallCards];
    lLargeCards=[self shuffleCards: self.largeCards];
    NSInteger nLarge= [self getNLarge];
    NSInteger nSmall=6-nLarge;
    
    // clear the number cards on display
    [self clearCards];

    // clear the target
    if(_running) [self setTargetTo: @""];
    
    // reset the progress indicator
    if(_running) [self resetTimer];
    
    // draw and display the small number cards (with a delay between each)
    for(NSInteger card=0; card<nSmall && _running; card++) {
        [[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:(NSTimeInterval)NUMBERDELAY]];
        if(_running) [self setCard: card to:[[lSmallCards objectAtIndex:card] integerValue]];
        [[self numbersForLili] addObject: [lSmallCards objectAtIndex:card]];
    }
    
    // draw and display the large number cards (with a delay between each)
    for(NSInteger card=0; card<nLarge && _running; card++) {
        [[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:(NSTimeInterval)NUMBERDELAY]];
        if(_running) [self setCard: card+nSmall to:[[lLargeCards objectAtIndex:card] integerValue]];
        [[self numbersForLili] insertObject: [lLargeCards objectAtIndex:card] atIndex:0];
    }
    
    // pause before showing the target
    [[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:(NSTimeInterval)NUMBERDELAY]];
    
    // last, generate the target number and write it into the target. We use a loop to show some
    // flickering numbers before showing the final target.
    for(int i=0; i<FLICKERTIMES && _running; i++) {
        [[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:(NSTimeInterval)FLICKERDELAY]];
        [self setTarget: [self getTarget]];
        if(_running) [self setTargetTo: [NSString stringWithFormat:@"%3d",self.target]];
    }
    
    // start lili thinking
    if (_running) [self performSelectorInBackground:@selector(lili:) withObject:nil];
    
    // start the timer
    // Now, update the progress bar smoothly over 30 seconds
    if(_running) [self runTimer];
}

// get a target number between 100 and 999
- (int)getTarget
{
    return random()%900+100;
}

- (void) updateProgress
{
    [_timer setProgress:(float)(_timerProgress+1)/TIMERSTEPS];
}
- (void) timesUp
{
    UIColor *red=[UIColor colorNamed: @"TimesUpRed"];
    [_mainView setBackgroundColor: red];
    [self setRunning:false];
}
- (void) runTimer
{
    [self setRunning: true];
    _timerProgress=0;
    NSTimer *timer=
    [NSTimer timerWithTimeInterval:TIMERSTEPDELAY repeats:true block:^(NSTimer *timer){
        _timerProgress++;
        if(_timerProgress<TIMERSTEPS && _running) {
            [self  performSelectorOnMainThread:@selector(updateProgress) withObject:nil waitUntilDone:false];
        } else {
            if(_running)
                [self   performSelectorOnMainThread:@selector(timesUp) withObject:nil waitUntilDone:false];
            [timer invalidate];
        }
    }];
    [self setActualTimer: timer];
    [timer setTolerance:TIMERSTEPDELAY/2];
    [[NSRunLoop currentRunLoop] addTimer: timer forMode:NSRunLoopCommonModes];
}
- (void) resetTimer
{
    [_actualTimer invalidate];
    [_mainView setBackgroundColor: [UIColor systemBackgroundColor]];
    [_timer setProgress:0.0];
    [self setTimerProgress:0];
}

- (NSArray*)loadDeckOfSize: (int) size with:(int) first, ...
{
    va_list args;
    va_start(args, first);
    NSMutableArray *deck=[NSMutableArray arrayWithCapacity:size];
    int arg=first;
    for(int card=0; card<size; card++) {
        [deck addObject:[NSNumber numberWithInteger:(NSInteger)arg]];
        arg=va_arg(args, int);
    }
    
    return deck;
}
- (NSArray*)shuffleCards: (NSArray*) cards
{
    return [cards sortedArrayUsingFunction:randSort context:NULL];
}

NSInteger randSort(id num1, id num2, void *context)
{
    if (random()%2 == 0)
        return NSOrderedAscending;
    else
        return NSOrderedDescending;
}

// get lili searching while the timer is happening
- (void) lili: (id)param
{
    self.myLili.target=self.target;
    self.myLili.numbers=self.numbersForLili;
    self.myLili.timeout=LILITIMEOUT;
    self.myLili.findAll=false;
    if((self.liliSays=[self.myLili seek])) {
        // UI updates only allowed from the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.cmdAsk setHidden:FALSE];
        });
    }
}
- (IBAction)askLili:(id)sender
{
    // give an alert saying what Lili says
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Lili says:"
                                                                   message:self.liliSays
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [[[(UIView*)sender window] rootViewController] presentViewController:alert animated:YES completion:nil];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_cmdGo release];
    [_cmdAsk release];
    [_mainView release];
    [_targetView release];
    [_txtTarget release];
    [_timer release];
    [super dealloc];
}
- (NSInteger)getNLarge {
    return [_nLarge selectedRowInComponent:0];
}



@end
