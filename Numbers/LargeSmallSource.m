//
//  LargeSmallSource.m
//  Numbers
//
//  Created by Glen Prideaux on 16/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LargeSmallSource.h"

@implementation LargeSmallSource

- (id)init
{
    self = [super init];
    if (self) {
        NSLog(@"LargeSmallSource initialized");
        // Initialize Data
    }
    
    return self;
}

// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    // only ever a single component
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    // we have five rows 
    return 5;
}

// The data to return for the row and component (column) that's being passed in
 - (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return @[@"Rat pack: 6 small",
             @"Adventurer: 1 large, 5 small",
             @"Family mix: 2 large, 4 small",
             @"Perfect match: 3 large, 3 small",
             @"Heavyweight: 4 large, 2 small"][row];
}

@end
