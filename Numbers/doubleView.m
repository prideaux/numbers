//
//  doubleView.m
//  Numbers
//
//  Created by Glen Prideaux on 11/4/17.
//
//

#import "doubleView.h"

@implementation doubleView
@synthesize upperView, lowerView;


- (void) viewWillTransitionToSize: (CGSize) size  withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
 {
     // If width > height,
     //     move upperView to the left edge, make its width half my width, make its height my height
     //     move lowerView to the right edge, make its width half my width, make its height my height
     // otherwise
     //     move upperView to the top edge, make its width my width, make its height half my height
     //     move lowerView to the bottom edge, make its width my width, make its height half my height
     
     NSLog(@"viewWillTransitionToSize:(%fx%f) withTransitionCoordinator:  called\n",size.width,size.height);
     [coordinator animateAlongsideTransition: ^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
         [self adjustToSize:size];
     } completion: nil];
     [super viewWillTransitionToSize: size withTransitionCoordinator: coordinator];
 }

- (void)dealloc
{
    [upperView release];
    [lowerView release];
    [_enclosingView release];
    [super dealloc];
}

// I don't understand why viewWillAppear doesn't take care of this properly
// on initial startup, but it doesn't.
- (void)viewDidLoad
{
    NSLog(@"viewDidLoad called\n");
    [super viewDidLoad];
    [self adjustToSize: self.enclosingView.frame.size];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear called\n");
    [super viewWillAppear:animated];
    [self adjustToSize: self.enclosingView.frame.size];
}
- (void)adjustToSize: (CGSize)size
{
    NSLog(@"adjustToSize:(%fx%f) called",size.width,size.height);
    CGFloat w, h, t, l;
    l=self.enclosingView.frame.origin.x;
    t=self.enclosingView.frame.origin.y;
    if(size.width > size.height) {
        w=0.5*size.width;
        h=size.height;
    } else {
        w=size.width;
        h=0.5*size.height;
    }
    upperView.frame = CGRectMake(l,t,w,h);
    lowerView.frame = CGRectMake(l+size.width-w,t+size.height-h,w,h);
}
@end
