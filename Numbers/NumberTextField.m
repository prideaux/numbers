//
//  NumberTextField.m
//  Numbers
//
//  Created by Glen Prideaux on 12/4/17.
//
//
// The sole purpose for this class is that iPad automatic resizing of these text fields is buggy
// (as of 12 April 2017) so that when the iPad rotates the width of the fields is not properly changed
// when the container view resizes and they end up overlapping each other.

#import "NumberTextField.h"

@implementation NumberTextField

- (void) viewWillTransitionToSize: (CGSize) size  withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition: ^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
    } completion: nil];
    [super viewWillTransitionToSize: size withTransitionCoordinator: coordinator];
}

@end
