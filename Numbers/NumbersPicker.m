//
//  NumbersPicker.m
//  Numbers
//
//  Created by Glen Prideaux on 21/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "NumbersPicker.h"

/*
@implementation myCounter
@synthesize integerValue;
+ (myCounter*) newCounterWithInteger: (NSInteger) value
{
    myCounter* counter=[myCounter new];
    [counter setIntegerValue:value];
    return counter;
}
- (myCounter*) dec
{
    [self setIntegerValue: [self integerValue]-1];
    return self;
}
@end
*/
@implementation NumbersPicker
@synthesize numbersPicker;
@synthesize componentRows;
@synthesize fullDeck;

- (void) dealloc
{
    [numbersPicker release];
    [componentRows release];
    free(fullDeck) ;
    [super dealloc];
}
- (id) init 
{
    self = [super init];
    if (self) {
        // Initialization code here.
        self.fullDeck = malloc(sizeof(int)*14);
        [self makeFullDeck];
        [self setComponentRows: [NSMutableArray arrayWithCapacity:6]];
        for (NSInteger c=0; c<6; c++) {
            [self.componentRows addObject: [NSArray array]];
        }
        [self makeComponentRowsFrom:0];
    }
    
    return self;
}
+ (int) indexFromNumber: (int) number
{
    if(number<=10) return number-1;
    else return number/25+9;
}
+ (int) numberFromIndex: (int) index
{
    if(index<10) return index+1;
    else return (index-9)*25;
}
- (void) makeFullDeck
{
    for (NSUInteger n=0; n<=9; n++) {
        self.fullDeck[n]=2;
    }
    for (NSUInteger n=10; n<=13; n++) {
        self.fullDeck[n]=1;
    }
}
- (void) cloneFullDeckTo: (int*)target
{
    memcpy(target, self.fullDeck, sizeof(int)*14);
}

- (void) makeComponentRowsFrom:(int)start
{
    int deck[14];
    for(int component=start; component<6; component++) {
        [self cloneFullDeckTo:deck];
        for (NSUInteger c=0; c<component; c++) {
            NSNumber *value=[self getNumberForComponent:c];
            if (value) {
                int index=[NumbersPicker indexFromNumber: [value intValue]];
                deck[index] = deck[index] - 1;
            }
        }
        // now iterate over the deck and add the key to the array if the counter is not zero
        NSMutableArray *returnValue=[NSMutableArray arrayWithCapacity:14];
        for(int n=0; n<14;n++) {
            if (deck[n]>0) {
                [returnValue addObject:[NSNumber numberWithInt: [NumbersPicker numberFromIndex:n]]];
            }
        }
        [returnValue sortUsingSelector:@selector(compare:)];
        [self.componentRows replaceObjectAtIndex:component withObject:returnValue];
    }
}
- (NSNumber*) getNumberForComponent: (NSInteger)component
{
    NSNumber *value=nil;
    NSInteger row=[self.numbersPicker selectedRowInComponent:component]; 
    if (row>=0) {
        NSArray *componentRow=[self.componentRows objectAtIndex:component];
        if (row < [componentRow count]) {
            value=[componentRow objectAtIndex:row];
        }
    }
    return value;
}
- (NSArray*) getNumbers
{
    NSMutableArray *numbers=[NSMutableArray arrayWithCapacity:6];
    for (NSInteger component=0; component<6; component++) {
        NSNumber *value=[self getNumberForComponent:component];
        if (value) {
            [numbers addObject:value];
        }
    }
    return numbers;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    // six components
    return 6;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    assert(component < [[self componentRows] count]);
    return [[[self componentRows] objectAtIndex:component] count];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    assert(component < [[self componentRows] count]);
    assert(row < [[[self componentRows] objectAtIndex:component] count]);
    if(![view isKindOfClass: [UILabel class]]) view = [UILabel new];
    NSNumber* value = (NSNumber*)[[self.componentRows objectAtIndex:component] objectAtIndex:row];
    NSString *title = [value stringValue];
    [(UILabel*)view setText: title];
    [(UILabel*)view setTextColor:(value.integerValue>10)?[UIColor whiteColor]:[UIColor blackColor]];
    [(UILabel*)view setBackgroundColor:(value.integerValue>10)?[UIColor blackColor]:[UIColor whiteColor]];
//    [(UILabel*)view setFont:(value.integerValue>10)?[UIFont fontWithName:@"Baskerville" size:6.0]:[UIFont systemFontOfSize:12.0]];
    [(UILabel*)view setTextAlignment:NSTextAlignmentCenter];
    [(UILabel*)view setAdjustsFontSizeToFitWidth:TRUE];
    [view setFrame:CGRectMake(0,0,40,24)];
    return view;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self makeComponentRowsFrom:(int)component+1];
    for (NSInteger c=component+1; c<6; c++) {
        [numbersPicker reloadComponent:c];
    }
}

@end
